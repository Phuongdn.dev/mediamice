
$(document).ready(function(){
  $('.your-class1').slick({
    // autoplay:true,
    // autoplaySpeed:1500,
    // arrows:true,

    // infinite: true,
    // slidesToShow: 3,
    // slidesToScroll: 3


    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
  });

  $('.your-class2').slick({
	  autoplay:true,
	  autoplaySpeed:1500,
	  arrows:true,

	  infinite: true,
	  slidesToShow: 5,
	  slidesToScroll: 5
	});
});
