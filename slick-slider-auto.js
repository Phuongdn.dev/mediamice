$(document).ready(function(){
	$('.slick-slider-autoplay').slick({
	  autoplay:true,
	  autoplaySpeed:1500,
	  arrows:true,

	  infinite: true,
	  slidesToShow: 3,
	  slidesToScroll: 3
	});
});
